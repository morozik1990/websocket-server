use Mix.Config

config :logger,
  backends: [
    { LoggerFileBackend, :info_log },
    { LoggerFileBackend, :warn_log },
    { LoggerFileBackend, :error_log }
  ]

config :logger, :info_log,
  path: "/var/log/websocket/info.log",
  level: :info

config :logger, :warn_log,
  path: "/var/log/websocket/warn.log",
  level: :warn

config :logger, :error_log,
  path: "/var/log/websocket/error.log",
  level: :error

config :server,
  port: 8300

config :server,
  rabbit_login: "guest",
  rabbit_passw: 123,
  rabbit_ex: "main_ex",
  rabbit_queue: "main_q"
