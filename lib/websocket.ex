

defmodule SimpleQueue do
  require Logger
  require UUID
  require Jason

  use GenServer
  use AMQP

  def start_link(state \\ []) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def init(chan) do
    {:ok, chan}
  end

  @doc """
  Функции обратного вызова для GenServer
  """

  def handle_info(_, chan) do
    {:noreply, chan}
  end

  def handle_call({_ , _}, chan) do
    {:reply, chan}
  end

  def handle_cast({ :push, %{ "service" => s, "method" => m, "args" => a, :uuid => u } }, {chan, exchange, for_test}) do
    IO.puts(for_test)
    case Jason.encode(%{ "service" => s, "method" => m, "args" => a, :uuid => u }) do
      {:ok, json} ->
        Logger.info("send to rabbit: #{json}")
        AMQP.Basic.publish(chan, exchange, "", json)
      {:error, _} ->
        Logger.warn("tuple was not encoded")
    end
    {:noreply, {chan, exchange, for_test}}
  end

  def handle_cast({_ , _}, chan) do
    IO.puts(:any)
    {:noreply, chan}
  end

end

defmodule SimpleQueue.Supervisor do
  use Supervisor
  use AMQP

  def start() do
    login = Application.get_env(:server, :rabbit_login)
    password = Application.get_env(:server, :rabbit_passw)
    exchange = Application.get_env(:server, :rabbit_ex)
    {:ok, conn} = AMQP.Connection.open("amqp://#{login}:#{password}@localhost")
    {:ok, chan} = AMQP.Channel.open(conn)
    :ok = AMQP.Exchange.declare(chan, exchange)
    children = [
      %{
       id: :worker_one,
       start: {SimpleQueue, :start_link, {chan, exchange}},
      }
      #Supervisor.child_spec({SimpleQueue, {chan, exchange, "1"}}, id: :worker_one),
      #Supervisor.child_spec({SimpleQueue, {chan, exchange, "2"}}, id: :worker_two)
      #{SimpleQueue, {chan, exchange}},
    ]
    opts = [strategy: :one_for_one, name: SimpleQueue.Supervisor]
    Supervisor.start_link(children, opts)
  end
end


defmodule Websocket do
    @moduledoc """
    Documentation for Websocket.
    """
    @doc """
    Websocket.
    """

    require Logger
    require Socket
    require UUID
    require Jason

    def start(atom) do
        init_storage()
        pid = init_gen_server(atom)
        spawn_monitor(__MODULE__, :init_server, [pid])
        receive do
          {:DOWN, _ref, :process, _from_pid, reason} ->
            Logger.warn("websocket server was stoped from reason #{reason}")
            Logger.warn("websocket server restart")
            start(:restart)
          msg ->
            IO.puts(msg)
        end
    end

    def init_gen_server(atom) do
      case atom do
        :start ->
          SimpleQueue.Supervisor.start()
        :restart ->
      end
    end

    def init_storage() do
      if Enum.member?(:ets.all(), :sockets) == false do
        :ets.new(:sockets, [:set, :public, :named_table])
      else
        :ets.delete(:sockets)
        :ets.new(:sockets, [:set, :public, :named_table])
      end
    end

    def init_server(pid) do
      port = Application.get_env(:server, :port)
      server = Socket.Web.listen! port
      spawn_monitor(__MODULE__, :accept, [server, pid])
      receive do
        {:DOWN, _ref, :process, _from_pid, reason} ->
          Process.exit(self(), :kill)
      end
    end

    def accept(server, pid) do
      client = server |> Socket.Web.accept!
      client |> Socket.Web.accept!
      uuid = UUID.uuid1()
      :ets.insert(:sockets, { uuid, client})
      spawn(__MODULE__, :client_process, [client, uuid, "", pid])
      accept(server, pid)
    end

    def client_process(client, uuid, msg, pid) do
      case client |> Socket.Web.recv! do
        {:text, data} ->
          case decode(data, uuid) do
            {:ok, %{ "service" => s, "method" => m, "args" => a, :uuid => u }} ->
              Logger.info("send to genserver: #{data}")
              push(%{ "service" => s, "method" => m, "args" => a, :uuid => u })
            {:error, _} ->
              Logger.warn("send error to client: {\"error\":\"is not JSON or you have not sent attributs\"}")
              client |> Socket.Web.send!({:text, "{\"error\":\"is not JSON or you have not sent attributs\"}"})
          end
          client_process(client, uuid, "", pid)
        {:ping, _} ->
          client |> Socket.Web.send!({:pong, ""})
          client_process(client, uuid, "", pid)
        {:pong, _} ->
          client_process(client, uuid, "", pid)
        {:binary, _} ->
          client_process(client, uuid, "", pid)
        {:fragmented, what_is, data} ->
          case what_is do
            :text ->
              msg = msg <> data;
              client_process(client, uuid, msg, pid)
            :binary ->
              client_process(client, uuid, "", pid)
            :continuation ->
              msg = msg <> data;
              client_process(client, uuid, msg, pid)
            :end ->
              msg = msg <> data;
              case decode(data, uuid) do
                {:ok, %{ "service" => s, "method" => m, "args" => a, :uuid => u }} ->
                  Logger.info("send to genserver: #{data}")
                  push(%{ "service" => s, "method" => m, "args" => a, :uuid => u })
                {:error, _} ->
                  Logger.warn("send error to client: {\"error\":\"is not JSON or you have not sent attributs\"}")
                  client |> Socket.Web.send!({:text, "{\"error\":\"is not JSON or you have not sent attributs\"}"})
              end
              msg = ""
              client_process(client, uuid, msg, pid)
            end
        {:close, _, _ } ->
          :ets.delete(:sockets, uuid)
        :close ->
          :ets.delete(:sockets, uuid)
      end
    end

    defp push(elem), do: GenServer.cast(SimpleQueue, {:push, elem})

    @spec decode(json, uuid) :: {result} when json: nonempty_charlist(), uuid: nonempty_list(), result: tuple()
    def decode(json, uuid) when byte_size(json) > 0 do
      case Jason.decode(json) do
        {:ok, collection} ->
          case is_integer(collection) do
            :true ->
              {:error, %{}}
            :false ->
              define_microservice(Map.put(collection, :uuid, uuid))
          end
        {:error, _} ->
          {:error, %{}}
      end
    end

    def decode(json, uuid) when byte_size(json) == 0 do
      {:error, %{}}
    end

    @spec define_microservice(array) :: {result} when array: map(), result: tuple()
    def define_microservice(%{ "service" => s, "method" => m, "args" => a, :uuid => u })  do
      {:ok, %{ "service" => s, "method" => m, "args" => a, :uuid => u }}
    end

    def define_microservice(_)  do
      {:error, %{}}
    end
end
